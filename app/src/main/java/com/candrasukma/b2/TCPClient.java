package com.candrasukma.b2;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import static com.candrasukma.b2.AgeCalculateActivity.AGE;
import static com.candrasukma.b2.PalindromeActivity.PALINDROME;

/**
 * Created by candrasukma on 22/03/18.
 */

public class TCPClient {
    private static final String TAG = "TCPClient";
    private static final String ipServer = "192.168.1.151";
    private String sentence, modifiedSentence, type;
    private TCPClientListener mTcpClientListener;
    private AppCompatActivity mContext;

    public TCPClient(AppCompatActivity mContext, String sentence, String type) {
        this.sentence = sentence;
        this.type = type;
        this.mContext = mContext;
    }

    public void startSocket() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Socket clientSocket = new Socket(ipServer, 6789);
                    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    try {
                        if (sentence.equalsIgnoreCase("exit")) {
                            clientSocket.close();
                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mTcpClientListener.onTcpResultPalindrome(sentence);
                                    return;
                                }
                            });
                            return;
                        }
                        Log.e(TAG, type + "#" + sentence.toLowerCase());
                        outToServer.writeBytes(type + "#" + sentence.toLowerCase() + '\n');
                        modifiedSentence = inFromServer.readLine();
                        if (modifiedSentence.equalsIgnoreCase("ERROR")) {
                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mTcpClientListener.onTcpResultError(modifiedSentence + " From Server");
                                    return;
                                }
                            });
                        }
                        if (type.equalsIgnoreCase(PALINDROME)) {
                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mTcpClientListener.onTcpResultPalindrome(modifiedSentence);
                                    return;
                                }
                            });
                        }
                        if (type.equalsIgnoreCase(AGE)) {
                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mTcpClientListener.onTcpResultAge(Double.parseDouble(modifiedSentence));
                                    return;
                                }
                            });

                        }
                    } catch (final Exception e) {
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mTcpClientListener.onTcpResultError(e.getMessage());
                                return;
                            }
                        });
                        e.printStackTrace();
                    }
                } catch (final Exception e) {
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mTcpClientListener.onTcpResultError(e.getMessage());
                            return;
                        }
                    });
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void setTcpClientListener(TCPClientListener mTcpClientListener) {
        this.mTcpClientListener = mTcpClientListener;
    }
}
