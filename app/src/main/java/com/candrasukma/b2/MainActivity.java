package com.candrasukma.b2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView txtPalindrome;
    TextView txtAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        txtPalindrome = (TextView) findViewById(R.id.main_palindrome);
        txtAge = (TextView) findViewById(R.id.main_age);

        txtAge.setOnClickListener(this);
        txtPalindrome.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.main_age:
                i = new Intent(this, AgeCalculateActivity.class);
                startActivity(i);
                break;
            case R.id.main_palindrome:
                i = new Intent(this, PalindromeActivity.class);
                startActivity(i);
                break;
        }
    }
}
