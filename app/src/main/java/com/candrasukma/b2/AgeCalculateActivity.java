package com.candrasukma.b2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class AgeCalculateActivity extends AppCompatActivity implements View.OnClickListener, TCPClientListener {

    public String TAG = this.getClass().getSimpleName();
    public static String AGE = "AGE";

    TCPClient tcpClient;
    TextView textOut, textOutDesc;
    EditText textIn;
    Button btnSend;
    RadioGroup radioPlanet;
    RadioButton radioPlanetSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_age_calculate);
        initView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                sendAction();
                break;
        }
    }

    @Override
    public void onTcpResultAge(double result) {
        textOut.setText(String.valueOf(result));
        textOutDesc.setText("Your age on " + radioPlanetSelected.getText().toString() + " is ");
        textIn.setText("");
    }

    @Override
    public void onTcpResultPalindrome(String result) {

    }

    @Override
    public void onTcpResultError(String result) {
        textOut.setText(result);
        textOutDesc.setText("");
        textIn.setText("");
    }

    private void initView() {
        textOutDesc = (TextView) findViewById(R.id.text_age_desc);
        textOut = (TextView) findViewById(R.id.text_age_other);
        textIn = (EditText) findViewById(R.id.text_in);
        btnSend = (Button) findViewById(R.id.btn_send);
        radioPlanet = (RadioGroup) findViewById(R.id.radioPlanet);
        btnSend.setOnClickListener(this);
    }

    private void sendAction() {
        int selectedId = radioPlanet.getCheckedRadioButtonId();
        radioPlanetSelected = (RadioButton) findViewById(selectedId);
        if (radioPlanetSelected == null) {
            Toast.makeText(this, "select planet!", Toast.LENGTH_LONG).show();
            return;
        }
        String word = textIn.getText().toString().trim();
        if (word.isEmpty()) {
            Toast.makeText(this, "fill blank form!", Toast.LENGTH_LONG).show();
            return;
        }
        tcpClient = new TCPClient(this, word + "#" + radioPlanetSelected.getText().toString(), AGE);
        tcpClient.setTcpClientListener(this);
        tcpClient.startSocket();
    }
}
