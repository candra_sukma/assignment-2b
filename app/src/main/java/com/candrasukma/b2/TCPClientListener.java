package com.candrasukma.b2;

/**
 * Created by candrasukma on 23/03/18.
 */

public interface TCPClientListener {
    void onTcpResultAge(double result);

    void onTcpResultPalindrome(String result);

    void onTcpResultError(String result);
}
