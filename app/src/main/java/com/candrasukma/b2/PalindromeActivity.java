package com.candrasukma.b2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PalindromeActivity extends AppCompatActivity implements TCPClientListener, View.OnClickListener {
    public String TAG = this.getClass().getSimpleName();
    public static String PALINDROME = "PALINDROME";

    TCPClient tcpClient;
    TextView textOut;
    EditText textIn;
    Button btnSend;
    String outPrint = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palindrome);
        initView();
    }

    @Override
    public void onTcpResultAge(double result) {

    }

    @Override
    public void onTcpResultError(String result) {
        outPrint = outPrint + "the " + textIn.getText().toString() + " result is " + result + "\n";
        textOut.setText(outPrint);
        textIn.setText("");
        Log.e(TAG, result);
    }

    @Override
    public void onTcpResultPalindrome(String result) {
        if (result.equalsIgnoreCase("exit")) {
            outPrint = outPrint + "Connection closed \n";
        } else {
            outPrint = outPrint + "the " + textIn.getText().toString() + " result is " + result + "\n";
        }
        textOut.setText(outPrint);
        textIn.setText("");
        Log.e(TAG, result);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                sendAction();
                break;
        }
    }

    private void initView() {
        textOut = (TextView) findViewById(R.id.text_out);
        textIn = (EditText) findViewById(R.id.text_in);
        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);
    }

    private void sendAction() {
        String word = textIn.getText().toString().trim();
        if (word.isEmpty()) {
            Toast.makeText(this, "fill blank message!", Toast.LENGTH_LONG).show();
            return;
        }
        tcpClient = new TCPClient(this, word, PALINDROME);
        tcpClient.setTcpClientListener(this);
        tcpClient.startSocket();
    }
}
